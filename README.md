Plot ohlc using data of websocket. (Drawing stops when websocket stops)

### one sec ohlc of btc
* bitflyer: https://bitbot125.gitlab.io/lightweight_charts_test/index.html
* bitmex: https://bitbot125.gitlab.io/lightweight_charts_test/bitmex.html
* binance futures: https://bitbot125.gitlab.io/lightweight_charts_test/binancef.html
* ftx: https://bitbot125.gitlab.io/lightweight_charts_test/ftx.html
* bybit: https://bitbot125.gitlab.io/lightweight_charts_test/bybit.html
* coinbase: https://bitbot125.gitlab.io/lightweight_charts_test/coinbase.html

To change ohlc to another second, change a value of SECOND_PERIOD in the html file.
